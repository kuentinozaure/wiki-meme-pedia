const express = require("express");
const cors = require("cors");
const dotenv = require("dotenv");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");

const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
dotenv.config();

//  wikimemepedia
// iKkWde0wY5eaheNO
// wiki-meme-pedia

const DATABASE_USER = process.env.DATABASE_USER;
const DATABASE_PASWORD = process.env.DATABASE_PASWORD;
const DATABASE_NAME = process.env.DATABASE_NAME;

// mongoose connection
// ?retryWrites=true&w=majority
const uri = `mongodb+srv://${DATABASE_USER}:${DATABASE_PASWORD}@cluster0.xya4n.mongodb.net/${DATABASE_NAME}`;
mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

const db = mongoose.connection;

const Meme = new mongoose.Schema({
  image: String,
  like: String,
  postedBy: String,
  information: String,
  tags: String,
});

const User = new mongoose.Schema({
  login: String,
  password: String,
  profilImage: String,
  mail: String,
  point: String,
});

const memeTable = mongoose.model("meme", Meme, "meme");
const userTable = mongoose.model("user", User, "user");

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => {
  console.log("connected to mongo");
});

app.get("/trending-meme", async (req, res) => {
  const memes = await memeTable
    .find({}, null, { sort: { like: "desc" } })
    .limit(5);
  try {
    res.status(200).send(memes);
  } catch (err) {
    res.status(500).send(err);
  }
});

app.put("/user", async (req, res) => {
  if (req.body === {} || req.body.user === {} || req.body.user === undefined) {
    res.status(404).send({ error: `Please provide user` }).end();
  }
  user = req.body.user;

  userTable.findOneAndUpdate(
    { _id: user._id },
    user,
    { upsert: true },
    (err, doc) => {
      if (err) return res.send(400, { error: err });
      return res.status(200).send("Succesfully saved.");
    }
  );
});

app.put("/meme", async (req, res) => {
  
  if (req.body === {} || req.body.meme === {} || req.body.meme === undefined) {
    res.status(404).send({ error: `Please provide meme` }).end();
  }
  meme = req.body.meme;
  memeTable.findOneAndUpdate(
    { _id: meme._id },
    meme,
    { upsert: true },
    (err, doc) => {
      if (err) return res.send(400, { error: err });
      return res.status(200).send("Succesfully saved.");
    }
  );
});

app.get("/meme/:id", async (req, res) => {
  if (req.params.id === "" || req.params.id === undefined) {
    res
      .status(404)
      .send({ error: `Please provide an user to get his datas` })
      .end();
  }
  userId = req.params.id;
  const memesOfUser = await memeTable.find({ postedBy: userId });
  try {
    res.status(200).send(memesOfUser);
  } catch (err) {
    res.status(500).send(err);
  }
});

app.delete("/meme/:id", async (req, res) => {
  if (req.params.id === "" || req.params.id === undefined) {
    res
      .status(404)
      .send({ error: `Please provide an user to delete this datas` })
      .end();
  }
  memeId = req.params.id;
  memeTable.findByIdAndDelete(memeId, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(`delete successful`);
    }
  });
});

app.post("/meme", async (req, res) => {
  if (req.body === {} || req.body.meme === {} || req.body.meme === undefined) {
    res.status(404).send({ error: `Please provide user to insert` }).end();
  }
  meme = req.body.meme;
  const memeElem = new memeTable(meme);
  memeElem.save((err) => {
    if (err)
      res.status(404).send({ error: `impossible to insert element` }).end();
    return res.status(200).send("Succesfully  inserted");
  });
});

app.post("/user", async (req, res) => {
  if (req.body === {} || req.body.user === {} || req.body.user === undefined) {
    res.status(404).send({ error: `Please provide user to insert` }).end();
  }
  user = req.body.user;

  pwd = user.password;

  user.password = require("crypto").createHash("sha256").update(pwd).digest("base64")

  const userElem = new userTable(user);
  userElem.save((err,usr) => {
    if (err)
      res.status(404).send({ error: `impossible to insert element` }).end();
      return res.status(200).send(usr);
  });
});


app.put("/user", async (req, res) => {
  if (req.body === {} || req.body.user === {} || req.body.user === undefined) {
    res.status(404).send({ error: `Please provide user` }).end();
  }
  user = req.body.user;

  userTable.findOneAndUpdate(
    { _id: user._id },
    user,
    { upsert: true },
    (err, doc) => {
      if (err) return res.send(400, { error: err });
      return res.status(200).send("Succesfully saved.");
    }
  );
});

app.get("/random", async (req, res) => {
  const memes = await memeTable.findOne();
  try {
    res.status(200).send(memes);
  } catch (err) {
    res.status(500).send(err);
  }
});

app.post("/connection", async (req, res) => {
  if (req.body === {} || req.body.login === "" || req.body.password === "") {
    res.status(404).send({ error: `Please provide your credentials` }).end();
  }

  const login = req.body.login;
  const password = req.body.password;
  const user = await userTable.find({ login: login });

  if (user.length === 0 || user === undefined) {
    res.status(404).send({ error: `Your credentials is not good` });
  }

  if (
    user[0].login === login &&
    user[0].password ===
      require("crypto").createHash("sha256").update(password).digest("base64")
  ) {
    res.status(200).send({
      success: `successful connected`,
      user: user[0],
    });
  }

  if (
    user[0].login !== login &&
    user[0].password !==
      require("crypto").createHash("sha256").update(password).digest("base64")
  ) {
    res.status(404).send({ error: `Your credentials is not good` });
  }
});

app.listen(process.env.PORT || 3000);
