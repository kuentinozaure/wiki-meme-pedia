import Vue from 'vue';
import './plugins/axios'
import App from './App.vue';

import Buefy from 'buefy';
import 'buefy/dist/buefy.css';

import VueRouter from 'vue-router';
import router from './router'

import '@mdi/font/css/materialdesignicons.css'

import Vuex from 'vuex'
import store from "./store/";

Vue.config.productionTip = false;
Vue.use(Buefy);
Vue.use(VueRouter);
Vue.use(Vuex)

new Vue({
  router,
  store:store,
  render: h => h(App)
}).$mount('#app');
