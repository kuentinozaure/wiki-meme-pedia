import Vue from "vue";
import VueRouter from "vue-router";
import HomePageComponent from "../components/HomePageComponent.vue";
import LoginComponent from "../components/shared/login/LoginComponent.vue";
import AccountComponent from "../components/shared/account/AccountComponent.vue";
import MyMemeComponent from "../components/shared/account/MyMemeComponent.vue";
import MemeComponent from "../components/shared/meme/MemeComponent.vue";
import RandomMemeComponent from "../components/shared/random/RandomMemeComponent";
import CreateAccountComponent from "../components/shared/account/CreateAccountComponent.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: HomePageComponent,
  },
  {
    path: "/login",
    name: "Login",
    component: LoginComponent,
  },
  {
    path: "/account",
    name: "Account",
    component: AccountComponent,
  },
  {
    path: "/new",
    name: "Meme",
    component: MemeComponent,
  },
  {
    path:"/mymeme",
    name: "MyMeme",
    component: MyMemeComponent,
  },
  {
    path:"/random",
    name: "Random",
    component: RandomMemeComponent,
  },
  {
    path:"/signup",
    name: "SignUp",
    component: CreateAccountComponent,
  },
  { 
    path: "*", 
    //component: PageNotFound 
  },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
];

const router = new VueRouter({
  routes,
});

export default router;
